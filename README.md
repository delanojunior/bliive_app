# Bliive App

https://media.giphy.com/media/7A805vOXJ8IENkzdbD/giphy.gif

## Instalações de Pacotes
```
npm install
```

### Executar Ambiente Desenvolvimento
```
npm run serve
```

### Executar Ambiente Produção
```
npm run build
```

### Observações
 - Para executar o projeto é necessário conexão de internet,
 devido ao uso de bibliotecas cdn.
